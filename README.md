# `botofu.ios`

Simple I/Os for the botofu project.

Includes fixed-size types and binary reader & writer.